#ifndef _APP_VERSION_H
#define _APP_VERSION_H

#define APP_VERSION_MAJOR    0
#define APP_VERSION_MINOR    2

#define APP_VERSION_BUILD    407

#endif // _APP_VERSION_H