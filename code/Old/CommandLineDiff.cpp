struct DiffPart_t
{
	dmp_operation_t operation;
	uint32_t strLength;
	const char* strPntr;
};

struct PartsList_t
{
	uint32_t numItems;
	DiffPart_t items[128];
};

static const char* OperationString(dmp_operation_t operation)
{
	switch (operation)
	{
		case DMP_DIFF_DELETE: return "Delete";
		case DMP_DIFF_EQUAL:  return "Equal";
		case DMP_DIFF_INSERT: return "Insert";
		default:              return "Unknown";
	}
}

static const char* OperationStr(dmp_operation_t operation)
{
	switch (operation)
	{
		case DMP_DIFF_DELETE: return "-";
		case DMP_DIFF_EQUAL:  return " ";
		case DMP_DIFF_INSERT: return "+";
		default:              return "Unknown";
	}
}

int AddToPartsList(void* reference, dmp_operation_t operation, const void* dataPntr, uint32_t dataLength)
{
	Assert(reference != nullptr);
	
	PartsList_t* listPntr = (PartsList_t*)reference;
	DiffPart_t* partPntr = &listPntr->items[listPntr->numItems];
	
	Assert(listPntr->numItems < ArrayCount(listPntr->items));
	
	partPntr->operation = operation;
	partPntr->strPntr = (const char*)dataPntr;
	partPntr->strLength = dataLength;
	listPntr->numItems++;
	
	return 0;
}

#define STRING1 "Hello Taylor Robbins!"
#define STRING2 "Hello Colby Robbins..."

int main()
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	PartsList_t partsList = {};
	char spaces[128] = {};
	memset(spaces, ' ', sizeof(spaces)-1);
	
	char strBuffer[256] = {};
	
	printf("Diffing \"%s\" against \"%s\"\n\n", STRING1, STRING2);
	
	dmp_diff* diff;
	
	if (dmp_diff_from_strs(&diff, NULL, STRING1, STRING2) == 0)
	{
		dmp_diff_foreach(diff, AddToPartsList, &partsList);
		
		printf("Got %u parts\n", partsList.numItems);
		
		printf(" ");
		for (uint32_t pIndex = 0; pIndex < partsList.numItems; pIndex++)
		{
			DiffPart_t* part = &partsList.items[pIndex];
			
			switch (part->operation)
			{
				case DMP_DIFF_EQUAL:
				{
					SetConsoleTextAttribute(consoleHandle, 0x03);//FOREGROUND_BLUE | FOREGROUND_INTENSITY);
					printf("%.*s", part->strLength, part->strPntr);
					
					memcpy(&strBuffer[strlen(strBuffer)], spaces, part->strLength);
				} break;
				
				case DMP_DIFF_DELETE:
				{
					if (pIndex+1 < partsList.numItems && 
						partsList.items[pIndex+1].operation == DMP_DIFF_INSERT)
					{
						DiffPart_t* nextPart = &partsList.items[pIndex+1];
						
						SetConsoleTextAttribute(consoleHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
						printf("%.*s", nextPart->strLength, nextPart->strPntr);
						
						memcpy(&strBuffer[strlen(strBuffer)], part->strPntr, part->strLength);
						
						if (nextPart->strLength > part->strLength)
						{
							memcpy(&strBuffer[strlen(strBuffer)], spaces, nextPart->strLength - part->strLength);
						}
						else if (part->strLength > nextPart->strLength)
						{
							printf("%.*s", part->strLength - nextPart->strLength, spaces);
						}
						
						//Jump over that next part
						pIndex++;
					}
					else
					{
						SetConsoleTextAttribute(consoleHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
						printf("%.*s", part->strLength, spaces);
						
						memcpy(&strBuffer[strlen(strBuffer)], part->strPntr, part->strLength);
					}
				} break;
				
				case DMP_DIFF_INSERT:
				{
					if (pIndex+1 < partsList.numItems && 
						partsList.items[pIndex+1].operation == DMP_DIFF_DELETE)
					{
						DiffPart_t* nextPart = &partsList.items[pIndex+1];
						
						SetConsoleTextAttribute(consoleHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
						printf("%.*s", part->strLength, part->strPntr);
						
						memcpy(&strBuffer[strlen(strBuffer)], nextPart->strPntr, nextPart->strLength);
						
						if (part->strLength > nextPart->strLength)
						{
							memcpy(&strBuffer[strlen(strBuffer)], spaces, part->strLength - nextPart->strLength);
						}
						else if (nextPart->strLength > part->strLength)
						{
							printf("%.*s", nextPart->strLength - part->strLength, spaces);
						}
						
						//Jump over that next part
						pIndex++;
					}
					else
					{
						SetConsoleTextAttribute(consoleHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
						printf("%.*s", part->strLength, part->strPntr);
						
						memcpy(&strBuffer[strlen(strBuffer)], spaces, part->strLength);
					}
				} break;
			};
		}
		
		printf("\n");
		SetConsoleTextAttribute(consoleHandle, FOREGROUND_RED | FOREGROUND_INTENSITY);
		printf("[%s]\n", strBuffer);
	}
	else
	{
		printf("Diff from strings failed!\n");
	}
	
	dmp_diff_free(diff);
	
	while(1);
	
	return 0;
}