/*
File:   appDefines.h
Author: Taylor Robbins
Date:   06\08\2017
*/

#ifndef _APP_DEFINES_H
#define _APP_DEFINES_H

#define SCROLLBAR_WIDTH    12
#define SCROLLBAR_PADDING  3

#define TAB_WIDTH          4
#define SCROLL_MULTIPLIER  40

#define Color_Background   NewColor(33, 33, 33, 255)
#define Color_Foreground   {0xFFF8F8F2}
#define Color_UiGray1      {0xFF494949}
#define Color_UiGray2      {0xFF404040}
#define Color_UiGray3      {0xFF303030}
#define Color_UiGray4      {0xFF101010}

#endif // _APP_DEFINES_H